#!/usr/bin/python


#Meghan Judge
#Rachel Kinney
#mjudge2@jhu.edu
#rkinney4@jhu.edu
#IMDB parser

from os import listdir
import re

scienceFictionMovies = dict()
scifiRatings = dict()
scifiVotes = dict()

def main():
    numSciFiMovies = 0
    numSciFiMoviesWithoutRatings = 0
    numSciFiMoviesWithoutVotes = 0
    numSkipped = 0

    #get list of data files
    rawdata = []
    for f in listdir('.'):
        if f[len(f)-4:] == '.txt':
            rawdata.append(f)

    #verify that there is data to parse
    if len(rawdata) == 0:
        print "Error: no data files found in current directory"
        return

    #Set up sql file
    sql = open("IMDB.sql", 'w')
    print "Initializing %s.... " % (sql.name)
    sql.write(initSql())

    print "Parsing from %d data files..." % (len(rawdata))

    #Calls the parsing methods for each table
    for f in rawdata:
        data = open(f, 'r')
        print ("   " + data.name + "..."),
        if data.name == 'genres.txt':
            parseGenres(data)
        if data.name == 'ratings.txt':
            parseRatings(data)
        if data.name == 'release-dates.txt':
            parseReleaseDates(data)

    print "Generating %s... \n" % (sql.name)

    #Writes insert instructions to the sql file.
    autoKey = 1
    for key, value in scienceFictionMovies.items():
        if '?' not in value:
            rating = 0
            votes = 0
            if key in scifiVotes:
                votes = scifiVotes[key]
                rating = scifiRatings[key]
            if key[0] is not '"':
                key = '"' + key + '"'
            if "-" not in value:
                value = value + "-01-01"
            date = '"' + value + '"'
            
            sql.write("INSERT IGNORE INTO ScienceFictionMovie values ")
            sql.write('({0}, {1}, {2}, {3}, {4});'.format(autoKey, date, key, rating, votes))
            sql.write("\n")
            autoKey = autoKey + 1
    else:
        numSkipped = numSkipped + 1

    sql.close()


    print "========Statistics========"
    print "Parsed %d science fiction movies..." %(len(scienceFictionMovies))
    print "%d science fiction movies did not have votes" %(len(scienceFictionMovies)-len(scifiVotes))
    print "%d science fiction movies did not have ratings" %(len(scienceFictionMovies)-len(scifiRatings))
    print "%d science fiction movies did not have release dates and were not inserted" %(numSkipped)


# REGEX: ^([\s\S]*)\(([\d{4}]*|\?*)(?:\/)?([\w])*?\)([\s\S]*?)([\s\S]*)$
# group(1): This extracts the name of the movie/show that appears before the date.
# group(2): The four-digit date appears between two parenthesis.
# group(3): Any additional additional content after the year
# group(4):
# group(5): The genre
# An indicator of whether the show was a tv show or movie may appear in parenthesis after the release year.
# The genre occurrs last.
def parseGenres( data ):
    x = 0
    showName = ""
    year = ""
    genre = ""
    for line in data:
        if "(V)" in line:
            line = line.replace("(V)", "")
        if "(VG)" in line:
            line = line.replace("(VG)", "")
        if "(TV)" in line:
            line = line.replace("(TV)", "")
        m = re.search(r"^([\s\S]*)\(([\d{4}]*|\?*)(?:\/)?([\w])*?\)([\s\S]*?)([\s\S]*)$", line)
        showName = m.group(1)
        year = m.group(2)
        genre = m.group(5)
        if "Sci-Fi" in genre:
            if showName not in scienceFictionMovies:
                scienceFictionMovies[showName] = year
    print "done"
    data.close()
    return

#Parses the imdb text file with the movies' ratings. Movies must be science fiction movies that were already extracted from the genres table.
#First, the method splits the string based on the location of whitespace.
#Then, it uses this regular expression: \(([\d{4}]*|\?*)(?:\/)?([\w])*?\) to ignore the release year that appears after the movie name.
#The movie name is then reconstructed.
def parseRatings( data ):
    showName = ""
    votes = ""
    rank = ""
    numInserted = 0

    for line in data:
        showName = ""
        pattern = re.compile("\(([\d{4}]*|\?*)(?:\/)?([\w])*?\)")
        line = re.split(ur"[\u200b\s]+", line, flags=re.UNICODE)
        if len(line) > 4:
            votes = line[2]
            rank = line[3]
            i = 4
            cont = True
            while i < len(line) and cont == True:
                if pattern.match(line[i]):
                    cont = False
                else:
                    showName = showName + line[i] + " "
                showName.rstrip()
                i = i+1
            if showName in scienceFictionMovies:
                if showName not in scifiRatings and showName not in scifiVotes:
                    scifiRatings[showName] = rank
                    scifiVotes[showName] = votes
                    numInserted = numInserted + 1

    print "done"
    data.close()
    return

#Parses the imdb text file with movies' release dates. Movies can only be released in the US and must be science fiction movies that were already extracted from the genres table.
#Uses the following regular expression to extract data from the line:
#^([\s\S]*)\(([\d{4}]*|\?*)(?:\/)?([\w])*?\)([\s\S]*) where:
#([\s\S]*) is the name of the show
#\(([\d{4}]*|\?*)(?:\/)?([\w])*?\) captures the release year, which is formatted like this: (2013). Alternative release year formats include: (2013/V) and (????).
#([\s\S]*) captures all data after the release date. This data contains the location of release and the precise date.
def parseReleaseDates( data ):
    showName = ""
    releaseDate = ""
    numUpdates = 0
    cn = 0
    for line in data:
        m = re.search(r"^([\s\S]*)\(([\d{4}]*|\?*)(?:\/)?([\w])*?\)([\s\S]*)", line)
        try:
            showName = m.group(1)
            releaseDate = m.group(4)
            #extracts the US release dates for science fiction movies
            if "USA:" in releaseDate:
                releaseDate = releaseDate.split("USA:", 1)[1]
                releaseDate = releaseDate.split("(", 1)[0]
                dateArr = releaseDate.split(" ")
                date = convertToDate(dateArr)
                if showName in scienceFictionMovies:
                    if 'None' not in date:
                        scienceFictionMovies[showName] = date
                        numUpdates = numUpdates + 1
        except:
            cn = cn + 1
    print "done"
    data.close()
    return

#Converts an array with up to three items into a numerical date.
#For example: January 01 2011 becomes 2011-01-01.
#If the only item in the array is a year, then the date is automatically set to be January 1st.
#If the only items in the array are the month and the year, then the day becomes the 1st of that month.
def convertToDate( dateArr ):
    dateMap = {'January': '01', 'February' : '02', 'March': '03', 'April': '04', 'May': '05', 'June': '06', 'July': '07', 'August': '08', 'September': '09', 'October': '10', 'November': '11', 'December': '12'}
    day = ""
    year = ""
    month = ""
    dateString = ""
    for item in dateArr:
        #Removes any non-date (year, month, date) items from the array
        if item not in dateMap:
            try:
                intItem = int(item)
            except:
                dateArr = dateArr.remove(item)
    #Determines whether the first item in the array is a date or a year by its value.
    #Otherwise, if it is a String, the month is converted to an integer.
    try:
        date = int(dateArr[0])
        if date > 0 and date < 32:
            day = date
        if date > 32:
            year = date
            dateString = year + "-01-01"
            return dateString
    except:
        month = dateMap.get(dateArr[0])
    #Determines whether the second item, if one exists, is a month or a year value.
    if len(dateArr) > 1:
        try:
            year = int(dateArr[1])
        except:
            month = dateMap.get(dateArr[1])
    #The only scenario where this clause is reached is when the date array contains the month, date, and year. Therefore, the integer is converted to a year.
    if len(dateArr) > 2:
        try:
            year = int(dateArr[2])
        except: pass

    if day == "":
        day = "01"
    if month == "":
        month = "01"
    dateString = str(year) + "-" + str(month) + "-" + str(day)
    return dateString

#Initializes the sql file with the query required to insert the ScienceFictionMovie table. The table contains information about science fiction movies only. There are five fields, each of which come from the different IMDB tables parsed in the above methods.
#MovieID is the auto-generated primary key for the record. It is an integer.
#ReleaseDate is the day on which the movie was released. This was extracted from the release-dates IMDB table.
#MovieTitle is the name of the science fiction movie. It was extracted from the genres IMDB table.
#Rating is the movie's rating on a scale from 0-10. This was extracted from the ratings IMDB table.
#NumberVotes is the number of individuals that voted on this movie.
def initSql():
    return ("/* Rachel Kinney and Meghan Judge\n" +
            "   rkinney4@jhu.edu\n   mjudge2@jhu.edu\n" +
            "   Databases Final Project - Parsed IMDB data\n" +
            "   Data information:\n" +
            "      Source: Internet Movie Database\n" +
            "      URL: http://www.imdb.com/\n" +
            "      Date collected: 12/10/2015 5:00 PM */\n\n" +
            "DROP TABLE IF EXISTS ScienceFictionMovie;\n" +
            "CREATE TABLE ScienceFictionMovie(\n" +
            "    MovieID       INTEGER not null,\n" +
            "    ReleaseDate          DATE,\n" +
            "    MovieTitle         TINYTEXT,\n" +
            "    Rating          FLOAT,\n" +
            "    NumberVotes     INTEGER,\n" +
            "    primary key (MovieID));\n\n")

if __name__ == "__main__":
    main()
