/* 1. Is there a correlation between UFO sightings (past 2011) and the 
      locations of Military Bases? (Past 2011, because the Military Base report
      was for 2011i */

delimiter //

DROP PROCEDURE IF EXISTS UFOSightingsMilitaryBasesCorrelation//
CREATE PROCEDURE UFOSightingsMilitaryBasesCorrelation()
BEGIN

DECLARE numBases1 INT;
DECLARE numBases2 INT;
DECLARE numSightings INT;

SELECT COUNT(SightingID) INTO numSightings FROM Sighting WHERE YEAR(Sighting.Date) = 2010;
SELECT COUNT(COMPONENT) INTO numBases1 FROM MILITARY_INSTALLATIONS_RANGES_TRAINING_AREAS_BND;
SELECT COUNT(COMPONENT) INTO numBases2 FROM MILITARY_INSTALLATIONS_RANGES_TRAINING_AREAS_PT;

CREATE TABLE state (
	code CHAR(2),
	name CHAR(35)
);

insert into state (code,name) values ('AL','Alabama');
insert into state (code,name) values ('AK','Alaska');
insert into state (code,name) values ('AS','American Samoa');
insert into state (code,name) values ('AZ','Arizona');
insert into state (code,name) values ('AR','Arkansas');
insert into state (code,name) values ('CA','California');
insert into state (code,name) values ('CO','Colorado');
insert into state (code,name) values ('CT','Connecticut');
insert into state (code,name) values ('DE','Delaware');
insert into state (code,name) values ('DC','District of Columbia');
insert into state (code,name) values ('FM','Federated States of Micronesia');
insert into state (code,name) values ('FL','Florida');
insert into state (code,name) values ('GA','Georgia');
insert into state (code,name) values ('GU','Guam');
insert into state (code,name) values ('HI','Hawaii');
insert into state (code,name) values ('ID','Idaho');
insert into state (code,name) values ('IL','Illinois');
insert into state (code,name) values ('IN','Indiana');
insert into state (code,name) values ('IA','Iowa');
insert into state (code,name) values ('KS','Kansas');
insert into state (code,name) values ('KY','Kentucky');
insert into state (code,name) values ('LA','Louisiana');
insert into state (code,name) values ('ME','Maine');
insert into state (code,name) values ('MH','Marshall Islands');
insert into state (code,name) values ('MD','Maryland');
insert into state (code,name) values ('MA','Massachusetts');
insert into state (code,name) values ('MI','Michigan');
insert into state (code,name) values ('MN','Minnesota');
insert into state (code,name) values ('MS','Mississippi');
insert into state (code,name) values ('MO','Missouri');
insert into state (code,name) values ('MT','Montana');
insert into state (code,name) values ('NE','Nebraska');
insert into state (code,name) values ('NV','Nevada');
insert into state (code,name) values ('NH','New Hampshire');
insert into state (code,name) values ('NJ','New Jersey');
insert into state (code,name) values ('NM','New Mexico');
insert into state (code,name) values ('NY','New York');
insert into state (code,name) values ('NC','North Carolina');
insert into state (code,name) values ('ND','North Dakota');
insert into state (code,name) values ('MP','Northern Mariana Islands');
insert into state (code,name) values ('OH','Ohio');
insert into state (code,name) values ('OK','Oklahoma');
insert into state (code,name) values ('OR','Oregon');
insert into state (code,name) values ('PW','Palau');
insert into state (code,name) values ('PA','Pennsylvania');
insert into state (code,name) values ('PR','Puerto Rico');
insert into state (code,name) values ('RI','Rhode Island');
insert into state (code,name) values ('SC','South Carolina');
insert into state (code,name) values ('SD','South Dakota');
insert into state (code,name) values ('TN','Tennessee');
insert into state (code,name) values ('TX','Texas');
insert into state (code,name) values ('UT','Utah');
insert into state (code,name) values ('VT','Vermont');
insert into state (code,name) values ('VI','Virgin Islands');
insert into state (code,name) values ('VA','Virginia');
insert into state (code,name) values ('WA','Washington');
insert into state (code,name) values ('WV','West Virginia');
insert into state (code,name) values ('WI','Wisconsin');
insert into state (code,name) values ('WY','Wyoming');

-- Stores for each state, what percentage of sightings occur in that state --
CREATE TABLE PercentageSightingsPerState AS
SELECT State, (COUNT(Sighting.SightingID)/numSightings)*100 AS NumberSightings
FROM Sighting_in, Sighting
WHERE Sighting_in.SightingID = Sighting.SightingID
AND Year(Sighting.Date) = 2010
GROUP BY State;

-- Stores for each state, what percentage of all military bases are in that state --
CREATE TABLE PercentageBasesPerStateName AS
SELECT a.STATE_TERR, (COUNT(a.COMPONENT)/(numBases1 + numBases2))*100 AS NumBases
FROM
((SELECT * FROM MILITARY_INSTALLATIONS_RANGES_TRAINING_AREAS_PT)
UNION ALL
(SELECT * FROM MILITARY_INSTALLATIONS_RANGES_TRAINING_AREAS_BND)) a
GROUP BY a.STATE_TERR;

-- Converts the states into their abbreviations --
CREATE TABLE PercentageBasesPerState AS
SELECT code as State, NumBases
FROM PercentageBasesPerStateName, state
WHERE PercentageBasesPerStateName.STATE_TERR = state.name;

-- Computes the average percentages for both sightings and bases --
SELECT (SUM(NumberSightings)/50) INTO @averageSightingVal FROM PercentageSightingsPerState;
SELECT AVG(NumBases) INTO @averageBaseVal FROM PercentageBasesPerState;

-- Computes and displays the difference from the averages for each state --
SELECT PercentageBasesPerState.State as State, ROUND((PercentageSightingsPerState.NumberSightings - @averageSightingVal), 2) as "Sightings %Diff from Average",
	ROUND((PercentageBasesPerState.NumBases - @averageBaseVal), 2) as "# Bases %Diff from Average"
FROM PercentageBasesPerState, PercentageSightingsPerState
WHERE PercentageBasesPerState.State = PercentageSightingsPerState.State
ORDER BY ROUND((PercentageSightingsPerState.NumberSightings - @averageSightingVal), 2) DESC;

DROP TABLE state;
DROP TABLE PercentageSightingsPerState;
DROP TABLE PercentageBasesPerState;
DROP TABLE PercentageBasesPerStateName;

END//

/* 2. Is there a correlation between UFO sightings and the release of Sci-Fi movies? */

DROP PROCEDURE IF EXISTS SightingSciFiMovieCorrelation//
CREATE PROCEDURE SightingSciFiMovieCorrelation()
BEGIN

CREATE TABLE AfterMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM ScienceFictionMovie, Sighting
WHERE DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) < 30
AND DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) > 0
GROUP BY MovieID;

CREATE TABLE BeforeMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM ScienceFictionMovie, Sighting
WHERE DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) < 0
AND DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) > -30
GROUP BY MovieID;

SELECT ROUND(AVG(AfterMovie.numSightings), 2) AS "Number of Sightings After",
	ROUND(AVG(BeforeMovie.numSightings), 2) AS "Number of Sightings Before",
	ROUND(((AVG(AfterMovie.numSightings) - AVG(BeforeMovie.numSightings))/AVG(BeforeMovie.numSightings))*100, 2) AS "Percent Change"
FROM AfterMovie, BeforeMovie
WHERE AfterMovie.MovieID = BeforeMovie.MovieID;

DROP TABLE IF EXISTS AfterMovie;
DROP TABLE IF EXISTS BeforeMovie;

END//

/* 3. Is there a correlation between UFO sightings and the release of Sci-Fi movies? */

DROP PROCEDURE IF EXISTS SightingSciFiMovieCorrelation//
CREATE PROCEDURE SightingSciFiMovieCorrelation()
BEGIN

-- Sightings Before --
CREATE TABLE BeforeMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM ScienceFictionMovie, Sighting
WHERE DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) <= 0
AND DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) >= -30
GROUP BY MovieID;

-- Sightings After --
CREATE TABLE AfterMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM ScienceFictionMovie, Sighting
WHERE DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) <= 30
AND DateDiff(ScienceFictionMovie.ReleaseDate, Sighting.Date) >= 0
GROUP BY MovieID;

-- Before/After and % Change --
CREATE TABLE MovieBeforeAfter AS
SELECT B.numSightings AS "Month Before", A.numSightings AS "Month After",
(((A.numSightings-B.numSightings)/B.numSightings)*100) AS PercentChange
FROM BeforeMovie AS B, AfterMovie AS A
WHERE B.MovieID = A.MovieID;

SELECT ROUND(AVG(PercentChange), 2) AS "Average % Change"
FROM MovieBeforeAfter;

DROP TABLE IF EXISTS AfterMovie;
DROP TABLE IF EXISTS BeforeMovie;
DROP TABLE IF EXISTS MovieBeforeAfter;

END//

/* 4. Is there a correlation between UFO sightings and the release of alien
      or space movies? */

DROP PROCEDURE IF EXISTS AlienSpaceMovieCorrelation//
CREATE PROCEDURE AlienSpaceMovieCorrelation()
BEGIN

DROP TABLE IF EXISTS AlienSpaceMovie;
DROP TABLE IF EXISTS AfterMovie;
DROP TABLE IF EXISTS BeforeMovie;

CREATE TABLE AlienSpaceMovie AS
SELECT MovieID, ReleaseDate as MovieDate FROM ScienceFictionMovie
WHERE MovieTitle LIKE '%alien'
OR MovieTitle LIKE '%space';

CREATE TABLE BeforeMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM AlienSpaceMovie, Sighting
WHERE DateDiff(AlienSpaceMovie.MovieDate, Sighting.Date) <= 0
AND DateDiff(AlienSpaceMovie.MovieDate, Sighting.Date) >= -30
GROUP BY MovieID;

CREATE TABLE AfterMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM AlienSpaceMovie, Sighting
WHERE DateDiff(AlienSpaceMovie.MovieDate, Sighting.Date) <= 30
AND DateDiff(AlienSpaceMovie.MovieDate, Sighting.Date) >= 0
GROUP BY MovieID;

CREATE TABLE MovieBeforeAfter AS
SELECT B.numSightings AS "Month Before", A.numSightings AS "Month After",
(((A.numSightings-B.numSightings)/B.numSightings)*100) AS PercentChange
FROM BeforeMovie AS B, AfterMovie AS A
WHERE B.MovieID = A.MovieID;

SELECT ROUND(AVG(PercentChange), 2) AS "Average % Change"
FROM MovieBeforeAfter;

DROP TABLE IF EXISTS AlienSpaceMovie;
DROP TABLE IF EXISTS AfterMovie;
DROP TABLE IF EXISTS BeforeMovie;
DROP TABLE IF EXISTS MovieBeforeAfter;

END//

/* 5. Is there a correlation between UFO sightings and the release of *popular*
      Sci-Fi movies? */

DROP PROCEDURE IF EXISTS SightingsPopularFilms//
CREATE PROCEDURE SightingsPopularFilms()
BEGIN

DROP TABLE IF EXISTS AfterMovie;
DROP TABLE IF EXISTS BeforeMovie;
DROP TABLE IF EXISTS PopularMovies;
DROP TABLE IF EXISTS ModPopMovies;
DROP TABLE IF EXISTS UnpopularMovies;

CREATE TABLE PopularMovies(
	MovieID	INTEGER not null,
	ReleaseDate DATE
);
CREATE TABLE ModPopMovies(
	MovieID	INTEGER not null,
	ReleaseDate DATE
);
CREATE TABLE UnpopularMovies(
	MovieID	INTEGER not null,
	ReleaseDate DATE
);

-- Devide movies into categories --
INSERT INTO PopularMovies
SELECT MovieID, ReleaseDate FROM ScienceFictionMovie
WHERE NumberVotes >= 50000;

INSERT INTO ModPopMovies
SELECT MovieID, ReleaseDate FROM ScienceFictionMovie
WHERE NumberVotes < 50000
AND NumberVotes > 10000;

INSERT INTO UnpopularMovies
SELECT MovieID, ReleaseDate FROM ScienceFictionMovie
WHERE NumberVotes <= 10000;

-- Sightings before and after release for each category --
CREATE TABLE BeforeMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM PopularMovies, Sighting
WHERE DateDiff(PopularMovies.ReleaseDate, Sighting.Date) <= 0
AND DateDiff(PopularMovies.ReleaseDate, Sighting.Date) >= -30
GROUP BY MovieID;

CREATE TABLE AfterMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM PopularMovies, Sighting
WHERE DateDiff(PopularMovies.ReleaseDate, Sighting.Date) <= 30
AND DateDiff(PopularMovies.ReleaseDate, Sighting.Date) >= 0
GROUP BY MovieID;

CREATE TABLE PopMovieBeforeAfter AS
SELECT B.numSightings AS "Month Before", A.numSightings AS "Month After",
(((A.numSightings-B.numSightings)/B.numSightings)*100) AS PercentChange
FROM BeforeMovie AS B, AfterMovie AS A
WHERE B.MovieID = A.MovieID;

DROP TABLE BeforeMovie;
DROP TABLE AfterMovie;

CREATE TABLE BeforeMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM ModPopMovies, Sighting
WHERE DateDiff(ModPopMovies.ReleaseDate, Sighting.Date) <= 0
AND DateDiff(ModPopMovies.ReleaseDate, Sighting.Date) >= -30
GROUP BY MovieID;

CREATE TABLE AfterMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM ModPopMovies, Sighting
WHERE DateDiff(ModPopMovies.ReleaseDate, Sighting.Date) <= 30
AND DateDiff(ModPopMovies.ReleaseDate, Sighting.Date) >= 0
GROUP BY MovieID;

CREATE TABLE ModMovieBeforeAfter AS
SELECT B.numSightings AS "Month Before", A.numSightings AS "Month After",
(((A.numSightings-B.numSightings)/B.numSightings)*100) AS PercentChange
FROM BeforeMovie AS B, AfterMovie AS A
WHERE B.MovieID = A.MovieID;

DROP TABLE BeforeMovie;
DROP TABLE AfterMovie;

CREATE TABLE BeforeMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM UnpopularMovies, Sighting
WHERE DateDiff(UnpopularMovies.ReleaseDate, Sighting.Date) <= 0
AND DateDiff(UnpopularMovies.ReleaseDate, Sighting.Date) >= -30
GROUP BY MovieID;

CREATE TABLE AfterMovie AS
SELECT MovieID, COUNT(SightingID) AS numSightings
FROM UnpopularMovies, Sighting
WHERE DateDiff(UnpopularMovies.ReleaseDate, Sighting.Date) <= 30
AND DateDiff(UnpopularMovies.ReleaseDate, Sighting.Date) >= 0
GROUP BY MovieID;

CREATE TABLE UnpMovieBeforeAfter AS
SELECT B.numSightings AS "Month Before", A.numSightings AS "Month After",
(((A.numSightings-B.numSightings)/B.numSightings)*100) AS PercentChange
FROM BeforeMovie AS B, AfterMovie AS A
WHERE B.MovieID = A.MovieID;

-- Union the three together into one output table --
(SELECT "Popular" AS Category, ROUND(AVG(PercentChange), 2) AS "Avg % Change" FROM PopMovieBeforeAfter)
UNION
(SELECT "Moderate" AS Category, ROUND(AVG(PercentChange), 2) FROM ModMovieBeforeAfter)
UNION
(SELECT "Unpopular" AS Category, ROUND(AVG(PercentChange), 2) FROM UnpMovieBeforeAfter);

DROP TABLE AfterMovie;
DROP TABLE BeforeMovie;
DROP TABLE PopMovieBeforeAfter;
DROP TABLE ModMovieBeforeAfter;
DROP TABLE UnpMovieBeforeAfter;

END//

/* 6. Is there a correlation between UFO sightings and time of year */

DROP PROCEDURE IF EXISTS SightingsTimeOfYearCorrelation//
CREATE PROCEDURE SightingsTimeOfYearCorrelation()
BEGIN

DROP TABLE IF EXISTS SeasonSightings;

CREATE TABLE SeasonSightings AS
SELECT YEAR(DATE) AS year,
	CASE MONTH(DATE)
	WHEN 1 then 'Winter'
	WHEN 2 then 'Winter'
	WHEN 3 then 'Spring'
	WHEN 4 then 'Spring'
	WHEN 5 then 'Spring'
	WHEN 6 then 'Summer'
	WHEN 7 then 'Summer'
	WHEN 8 then 'Summer'
	WHEN 9 then 'Fall'
	WHEN 10 then 'Fall'
	WHEN 11 then 'Fall'
	WHEN 12 then 'Winter'
	END AS season,
	COUNT(SightingID) AS frequency
FROM Sighting
GROUP BY season, year;

SELECT season AS "Season", ROUND(avg(frequency/yearlySightings)*100, 2) AS "Avg % of Annual Sightings"
FROM SeasonSightings AS a,
	(SELECT sum(frequency) AS yearlySightings, year
	FROM SeasonSightings
	GROUP BY year) y
WHERE y.year = a.year
GROUP BY a.season;

DROP TABLE IF EXISTS SeasonSightings;

END//

/* 7. On what percentage of nights with full moons are UFOs seen each year?*/

DROP PROCEDURE IF EXISTS SightingsFullMoonCorrelation//

CREATE PROCEDURE SightingsFullMoonCorrelation()
BEGIN

-- Gets the set of sightings that took place during a full moon --
DROP TABLE IF EXISTS OnFullMoons;
CREATE TABLE OnFullMoons AS
SELECT SightingID, Date
FROM Sighting, FullMoon
WHERE Sighting.Date = FullMoon.MoonDate;

-- Overall monthly count of sightings --
DROP TABLE IF EXISTS MonthlyCount;
CREATE TABLE MonthlyCount AS
SELECT COUNT(SightingID) AS numSightings, MONTH(Date) AS M, YEAR(Date) AS Y
FROM Sighting
GROUP BY Y, M;

-- Calculates what % of a month's sightings occured during a full moon --
SELECT ROUND(AVG(Percentage), 2) AS "Avg. % of Month's Sightings that Occur on a Full Moon"
FROM (
    SELECT ((COUNT(OnFullMoons.SightingID)/MonthlyCount.numSightings)*100) AS Percentage , MonthlyCount.M, MonthlyCount.Y
    FROM OnFullMoons, MonthlyCount
    WHERE YEAR(OnFullMoons.Date) = MonthlyCount.Y AND MONTH(OnFullMoons.Date) = MonthlyCount.M
    GROUP BY MonthlyCount.Y, MonthlyCount.M) AS MonthlyPercentages;

DROP TABLE OnFullMoons;
DROP TABLE MonthlyCount;

END//

delimiter ;