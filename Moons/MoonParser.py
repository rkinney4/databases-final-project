#!/usr/bin/python

#Meghan Judge
#Rachel Kinney
#mjudge2@jhu.edu
#rkinney4@jhu.edu
#IMDB parser

from os import listdir
import re

def main():

    #get list of data files
    rawdata = []
    for f in listdir('.'):
        if f[len(f)-4:] == '.txt':
            rawdata.append(f)

    #verify that there is data to parse
    if len(rawdata) == 0:
        print "Error: no data files found in current directory"
        return

    print "Parsing from %d data files..." % (len(rawdata))

    #Calls the parsing methods for each table
    for f in rawdata:
        data = open(f, 'r')
        print ("   " + data.name + "..."),
        if data.name == 'FullMoons.txt':
            print "found"
            parseFullMoons(data)

#Parses the full moon data set.
#Extracts the date and time from the database and creates an insert statement in the sql file.
def parseFullMoons( data ):
    monthMap = {'Jan': '01', 'Feb' : '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}

    #Set up sql file
    sql = open("Moons.sql", 'w')
    print "Initializing %s.... " % (sql.name),
    sql.write(initSql())
    autoKey = 1
    numBadLines = 0
    numInserted = 0
    
    for line in data:
        line = re.split(ur"[\u200b\s]+", line, flags=re.UNICODE)
        try:
            date = int(line[0])
            formattedDate = '"' + line[0] + "-" + monthMap.get(line[1]) + "-" + line[2] + '"'
            time = '"' + line[3] + '"'

            #Writes insert instructions to the sql file.
            sql.write("INSERT IGNORE INTO FullMoon values ")
            sql.write('({0}, {1}, {2});'.format(autoKey, formattedDate, time))
            sql.write("\n")
            autoKey = autoKey + 1
            numInserted = numInserted + 1
        except:
            numBadLines = numBadLines + 1
            
            
    print "done"
    sql.close()
    data.close()
 
    print "\n========Statistics========"
    print "Inserted %d full moon records..." %(numInserted)
 
    return

#Initializes the sql file with the query required to insert the FullMoons table. The table contains dates and times of the full moons from 1900-2099.
#MoonID is the auto-generated primary key for the record. It is an integer.
#MoonDate is the day on which the full moon appeared.
#MoonTime is the time at which the full moon appeared.
def initSql():
    return ("/* Rachel Kinney and Meghan Judge\n" +
            "   rkinney4@jhu.edu\n   mjudge2@jhu.edu\n" +
            "   Databases Final Project - Parsed Moon data\n" +
            "   Data information:\n" +
            "      Source: Keith's Moon Page, JHU APL\n" +
            "      URL: http://home.hiwaay.net/~krcool/Astro/moon/fullmoon.htm\n" +
            "      Date collected: 12/11/2015 9:00 PM */\n\n" +
            "DROP TABLE IF EXISTS FullMoon;\n" +
            "CREATE TABLE FullMoon(\n" +
            "    MoonID       INTEGER not null,\n" +
            "    MoonDate     DATE,\n" +
            "    MoonTime     TIME,\n" +
            "    primary key (MoonID));\n\n")

if __name__ == "__main__":
    main()
