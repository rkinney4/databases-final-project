#!/usr/bin/python

""" Python program which parses all UFO data into a single sql file.
    Any data to be parsed must be contained in the same directory as this script
    and be an .html file whose name starts with "rawdata" of the format used
    here for example: http://www.nuforc.org/webreports/ndxe201511.html """

from os import listdir
from os.path import isfile
from sys import exit
from operator import itemgetter

def main():
    # get list of data files
    rawdata = []
    for f in listdir('.'):
        if f[:7] == 'rawdata' and f[len(f)-5:] == '.html':
            rawdata.append(f)
    # verify that there is data to parse
    if len(rawdata) == 0:
        print "Error: no data files found in current directory.\n"
        return

    # set up sql file
    sql = open("UFO.sql", 'w')
    print "Initializing %s... " % (sql.name),
    sql.write(initsql())
    # initialize values
    Sighting = []
    Location = set([])
    Sight_In = []
    sID = 0
    rejected = 0
    parsed = 0

    print "done"
    print "Parsing from %d data files..." % (len(rawdata))
    #run through each file
    for fname in rawdata:
        f = open(fname, 'r')
        print "   %s..." % (fname),
        line = f.readline()
        while line != "":
            if line[:4] == "<TD>":
                S_entry = []
                L_entry = []
                SI_entry = []
                sID += 1
                # fill entries
                S_entry.append(sID)
                SI_entry.append(sID)
                parseDate(S_entry, line)
                ret = parseLoc(L_entry,SI_entry, f.readline(), f.readline())
                parseShape(S_entry, f.readline())
                parsed += parseDur(S_entry, f.readline())
                parseSumm(S_entry, f.readline())
                f.readline()
                # add to sets
                if ret == 0:
                    Sighting.append(S_entry)
                    Location.add(tuple(L_entry))
                    Sight_In.append(SI_entry)
                else:
                    sID -= 1
                    rejected += 1
            line = f.readline()
        print "done"
    # Generate sql file
    print "Generating %s... " % (sql.name),
    # Generate Sighting Table
    sql.write("INSERT IGNORE INTO Sighting VALUES\n")
    for i in range(len(Sighting)):
        for j in range(len(Sighting[0])):
            if j != 0 and Sighting[i][j] != 'NULL':
                Sighting[i][j] = '\"' + Sighting[i][j] + '\"'
        sql.write('({0}, {1}, {2}, {3}, {4}, {5}),\n'.format(Sighting[i][0],
            Sighting[i][1], Sighting[i][2], Sighting[i][3], Sighting[i][4],
            Sighting[i][5]))
    sql.seek(-2,1)
    sql.write(";\n\n")
    # Generate Location Table
    Location = sorted(sorted(list(Location), key=itemgetter(0)), key=itemgetter(1))
    sql.write("INSERT IGNORE INTO Location VALUES\n")
    for i in range(len(Location)):
        Loc = [Location[i][0], Location[i][1]]
        for j in range(2):
            if Location[i][j] != 'NULL':
                Loc[j] = '\"' + Location[i][j] + '\"'
        sql.write('({0}, {1}),\n'.format(Loc[0], Loc[1]))
    sql.seek(-2,1)
    sql.write(";\n\n")
    # Generate Sighting_in Table
    sql.write("INSERT IGNORE INTO Sighting_in VALUES\n")
    for i in range(len(Sight_In)):
        for j in range(len(Sight_In[0])):
            if j != 0 and Sight_In[i][j] != 'NULL':
                Sight_In[i][j] = '\"' + Sight_In[i][j] + '\"'
        sql.write('({0}, {1}, {2}),\n'.format(Sight_In[i][0], Sight_In[i][1],
            Sight_In[i][2]))
    sql.seek(-2,1)
    sql.write(";")
    sql.close()
    print "done\n"
    # print parsing statistics
    total = sID + rejected
    print "      ------ Statistics ------"
    print "           Total sightings:     {0}".format(total)
    print "  Total sightings accepted: {0:.0f}% {1}".format((float(sID)/total)*100, sID)
    print "  Total sightings rejected: {0:.0f}% {1}".format((float(rejected)/total)*100, rejected)
    print "          Parsed durations: {0:.0f}% {1}".format((float(parsed)/total)*100, parsed)
    print "      Unparsable durations: {0:.0f}% {1}\n".format((float(total-parsed)/total)*100, total-parsed)

# takes date line from file and parses out the date and time of a sighting if
# it is possible to do so. Otherwise, sets values to NULL.
def parseDate(entry, line):
    """ note: There are 16 sightings in the database that are known to have been
        before 1900. For simplicity's sake, we have ignored parsing these values
        and have mixed them in with the 1900s and 2000s. Many of these sightings
        will be rejected anyway for being outside the US """
    # gets actual information
    line = line[line.index(".html>")+6:line.rindex("</A>")]
    # pulls apart date
    sub = line.split('/')
    if (len(sub) == 3 and sub[0].isdigit() and sub[1].isdigit() 
        and sub[2][:2].isdigit()):
        M = sub[0]
        D = sub[1]
        # separates date and time
        sub = sub[2].split()
        # parses date into proper YYYY MM DD formats
        if int(sub[0]) > 15:
            Y = '19' + sub[0]
        else:
            Y = '20' + sub[0]
        if int(M) < 10:
            M = '0' + M
        if int(D) < 10:
            D = '0' + D
        # if there is a time
        entry.append(Y + '-' + M + '-' + D)
        if len(sub) == 2:
            sub = sub[1]
        else:
            entry.append('NULL') # append null for missing time
            return
    else:
        entry.append('NULL') # append null for missing date
        if len(sub) > 1:
            sub = sub[len(sub)-1].split()
            if len(sub) == 2:
                sub = sub[1]
            else:
                sub = sub[0]
        else:
            sub = sub[0]
    if len(sub) > 5:
        sub = sub[len(sub)-5:]
    if (len(sub) == 5 and sub[2] == ':'):
        entry.append(sub)
    else:
        entry.append('NULL')

# Takes both the city and state lines provided and attempts to verify that they
# are within the US. Will reject otherwise and set to NULL if necessary.
def parseLoc(Loc, SightIn, cline, sline):
    # some countries we want to reject, though most whould be filtered out by
    # their state being '<BR>'
    countries = ['canada','germany','england', 'venezuela', 'spain', 'sweden',
                 'australia', 'greece', 'brazil', 'iraq', 'ireland', '(italy',
                 'africa', '(mexico', 'india', '(uk' , 'pakistan', 'philippines',
                 'morocco', 'puerto rico', 'dominican republic', 'new zealand']
    states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", 
              "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", 
              "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", 
              "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", 
              "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY", "NULL"]
    cline = cline[cline.index("000>")+4:cline.rindex("</TD>")]
    sline = sline[sline.index("000>")+4:sline.rindex("</TD>")]
    cline = cline.replace('<BR>', '')
    cline = cline.replace('?', '')
    # checks for countries matches
    for each in countries:
        if cline.lower().find(each) != -1:
            return -1
    # removes extra information from city name
    extra = ['(', '/', ',', '- ']
    for each in extra:
        ind = cline.find(each)
        if ind != -1:
            cline = cline[:ind].strip()
    # fills in null if the values are empty
    if cline == '':
        cline = 'NULL'
    if sline == '':
        sline = 'NULL'
    # checks that the state is valid
    if sline.upper() not in states:
        return -1
    Loc.append(cline)
    Loc.append(sline)
    SightIn.append(cline)
    SightIn.append(sline)
    return 0

# Takes the shape line provided, and parses it.
def parseShape(entry, line):
    line = line[line.index("000>")+4:line.rindex("</TD>")]
    line = line.replace('<BR>', '').strip()
    if line == '':
        line = 'NULL'
    entry.append(line)

# Takes the duration line provided and attempts to standardize it. If it is not
# possible to obtain some standard interpretation, or the duration is found to
# be too large, will set to NULL.
def parseDur(entry, line):
    flag = 0
    time = ['hour','hr','min','sec']
    line = line[line.index("000>")+4:line.rindex("</TD>")]
    
  # pre-processing
    line = line.lower()
    line = line.strip()
    line = line.replace(',', '.')
    line = line.replace('. ', ' ')
    line = line.replace('?', '')
    line = line.replace('--', '-')
    line = line.replace('  ', ' ')
    line = line.replace('split ', '')
    line = line.replace('plus', '+')
    # standardize misspellings
    spell = ['approx ', ' approx', 'seconds', 'minutes']
    line = line.replace('appr ', spell[0]).replace('aprox ', spell[0])
    line = line.replace('appox ', spell[0]).replace('appx ', spell[0])
    line = line.replace('apx ', spell[0]).replace('app ', spell[0])
    line = line.replace('aprx ', spell[0])
    line = line.replace(' apprx', spell[1]).replace(' aprox', spell[1])
    line = line.replace('secounds', spell[2])
    line = line.replace('minuts', spell[3])
    # handle keywords
    if line[:5] == 'still':
        entry.append('NULL')
        return 1
    if line.endswith('.'):
        line = line[:len(line)-1]
    keywords = [':', '+/- ', 'abt ', 'over ', 'about ', 'under ', 'maybe ', 
        'around ', 'approx ', 'at least ', 'less than ', 'less then ', 
        'more than ', 'approximately ', 'a ', 'an ']
    for word in keywords:
        if line[:len(word)] == word:
            line = line[len(word):]
    if line[:5] == 'hours' or line[:7] == 'minutes' or line[:7] == 'seconds':
        line = '2 ' + line
    if (line[:4] == 'hour' or line[:2] == 'hr' or line[:6] == 'minute' or 
        line[:3] == 'min' or line[:6] == 'second' or line[:3] == 'sec'):
        line = '1 ' + line
    if line.endswith(' +'):
        line = line[:len(line)-2]
    if line.endswith(' approx'):
        line = line[:len(line)-7]
    line = line.replace(' of ', ' ')
    line = line.replace(' or ', '-')
    line = line.replace(' to ', '-')
    line = line.replace(' an ', ' ')
    line = line.replace(' + ', ' ')
    line = line.replace(' -', '-')
    line = line.replace('- ', '-')
    line = line.replace('\\', '/')
    if line.find(' 1/2') > 0:
        line = line.replace(' 1/2', '.5')
    for t in time:
        ind = line.find(t)
        # add space if needed: Ex. 4min
        if ind > 0 and line[ind-1].isdigit():
            line = line[:ind] + ' ' + line[ind:]
        # fix '-' or '.' or '/' instead of ' '
        if ind > 1 and line[ind-2].isdigit() and (line[ind-1] == '-' or 
            line[ind-1] == '.' or line[ind-1] == '/'):
            line = line[:ind-1] + ' ' + line[ind:]
    line = line.strip()
    sub = line.split()
    # make single letters parsable
    for i in range(len(sub)):
        if sub[i] == 'h':
            sub[i] = sub[i] + 'r'
        if sub[i] == 'm':
            sub[i] = sub[i] + 'in'
        if sub[i] == 's':
            sub[i] = sub[i] + 'ec'

  # Parse if possible
    if len(sub) > 2:
        # if more than 2 parts but the end is irrelevant
        num = ''
        for i in range(len(sub)-2):
            for j in time:
                if sub[i+2].find(j) != -1 or sub[i+2].isdigit():
                    num = 'X'
        if num == '':
            sub = sub[:2]

    if len(sub) == 4:
        num1 = durParsePair([sub[0],sub[1]])
        num2 = durParsePair([sub[2],sub[3]])
        if num1 == 'NULL' or num2 == 'NULL':
            entry.append('NULL')
            return 1
        elif num1 != '0' and num2 != '0':
            sub = [num1[:2],num1[3:5],num1[6:]]
            if int(num1[:2]) < int(num2[:2]):
                sub[0] = num2[:2]
            if int(num1[3:5]) < int(num2[3:5]):
                sub[1] = num2[3:5]
            if int(num1[6:]) < int(num2[6:]):
                sub[2] = num2[6:]
            entry.append(sub[0] + ':' + sub[1] + ':' + sub[2])
            return 1
        elif num1 != '0':
            entry.append(num1)
            return 1
        elif num2 != '0':
            entry.append(num2)
            return 1
    if len(sub) >= 2:
        # try the first 2 parts
        num = durParsePair([sub[0],sub[1]])
        if num != '0':
            entry.append(num)
            return 1
        # try the last 2 parts
        num = durParsePair([sub[len(sub)-2],sub[len(sub)-1]])
        if num != '0':
            entry.append(num)
            return 1
        # try to find an already well-formated pair somewhere
        for i in range(len(sub)-1):
            if sub[i].isdigit():
                num = durParsePair([sub[i],sub[i+1]])
                if num != '0':
                    entry.append(num)
                    return 1
    if len(sub) == 1:
        if sub[0].endswith('am') or sub[0].endswith('pm'):
            sub[0] = sub[0][:len(sub[0])-2]
        num = sub[0].split(':')
        if len(num) == 1:
            num = sub[0].split('+')
        if (sub[0].isdigit() or # just a single number
            (len(num) == 2 and num[0].isdigit() and num[1].isdigit())): 
            entry.append('NULL')
            return 1
        elif (len(num) == 2 and num[0].isdigit() and not num[1].isdigit()):
            num = durParsePair([num[0],num[1]])
            if num != '0':
                entry.append(num)
                return 1
    # check if anything to go on, else reject
    num = ''
    for i in range(len(sub)):
        for j in time:
            ind = sub[i].find(j)
            if ind != -1 and not (ind != 0 and sub[i][ind-1].isalpha()):
                num = 'X'
    if num == '':
        entry.append('NULL')
        return 0
  # Unable to parse, reject
    entry.append('NULL')
    return 0
    
# Helper function for parseDur() that takes a list of two strings and attmepts
# to interpret them as a number time pair. Ex. ['5', 'minutes']
def durParsePair(sub):
    # if the first part is not a number, try to make it one
    if not sub[0].isdigit():
        # convert few, several, couple, half
        keywords = {'a': '1', 'an': '1', 'couple': '2', 'few': '3',
            'several': '4', 'half': '0.5', 'one': '1', 'two': '2', 'three': '3',
            'four': '4', 'five': '5', 'six': '6', 'seven': '7', 'eight': '8',
            'ten': '10', 'fifteen': '15', 'twenty': '20', 'thirty': '30', 
            'sixty': '60'}
        for key in keywords:
            if sub[0] == key:
                sub[0] = keywords[key]
        # shave off attached word prefixes: Ex. over2 hours -> 2 hours
        if sub[0][:2].isalpha() and sub[0][len(sub[0])-1:].isdigit():
            for i in range(len(sub[0])):
                if sub[0][i].isdigit():
                    break
            sub[0] = sub[0][i:]
        # fix 0 vs o 
        ind = sub[0].find('o')
        if (ind > 0 and ind == len(sub[0])-1 and sub[0][ind-1].isdigit()):
            sub[0] = sub[0][:ind] + '0' + sub[0][ind+1:]
        # Shave off prefix symbols: Ex. +4 -> 4
        while (len(sub[0]) >= 1 and (sub[0][0] == '~' or sub[0][0] == '>' or sub[0][0] == '<'
            or sub[0][0] == '+' or sub[0][0] == '-' or sub[0][0] == '@')):
            sub[0] = sub[0][1:]
        # Shave off suffix symbols: Ex. 4+ -> 4
        if (sub[0].endswith('~') or sub[0].endswith('>') or
            sub[0].endswith('<') or sub[0].endswith('+')):
            sub[0] = sub[0][:len(sub[0])-1]
        # round decimals
        num = sub[0].split('.')
        if num[0] == '':
            num[0] = '0'
        if len(num) == 2 and num[0].isdigit() and num[1].isdigit():
            if int(num[1]) == 0:
                sub[0] = num[0]
            else:
                sub[0] = str(int(num[0]) + 1)
        elif len(num) == 2 and num[1].isdigit():
            sub[0] = num[1]
        # break ranges: Ex. 1-2 -> 1
        num = sub[0].split('-')
        if len(num) == 1:
            num = sub[0].split('~')
        if len(num) == 2:
            for key in keywords:
                for i in range(2):
                    if num[i] == key:
                        num[i] = keywords[key]
            if num[0].isdigit() and num[1].isdigit():
                sub[0] = min(num)
            if num[0].isdigit():
                sub[0] = num[0]
            elif num[1].isdigit():
                sub[0] = num[1]         
        num = sub[0].split('/')
        if len(num) == 2 and num[0].isdigit() and num[1].isdigit():
            sub[0] = min(num)
        num = sub[0].split('to')
        if len(num) == 2 and num[0].isdigit() and num[1].isdigit():
            sub[0] = min(num)
        num = sub[0].split('or')
        if len(num) == 2 and num[0].isdigit() and num[1].isdigit():
            sub[0] = min(num)
        num = sub[0].split(':')
        if len(num) == 2 and num[0].isdigit():
            sub[0] = num[0]
    # if the first part is a number, try to parse it
    if sub[0].isdigit():
        # reject month time spans
        if sub[1].find('month') != -1:
            return 'NULL'
        # pad with 0s appropriately
        if int(sub[0]) < 10:
            sub[0] = '0' + sub[0]
        if sub[1][:3] == 'sec':
            if int(sub[0]) >= 60:
                sub[0] = str(int(sub[0]) / 60)
                sub[1] = 'min'
            else:
                return '00:00:' + sub[0]
        if sub[1][:3] == 'min':
            if int(sub[0]) >= 60:
                sub[0] = str(int(sub[0]) / 60)
                sub[1] = 'hour'
            else:
                return '00:' + sub[0] + ':00'
        if (sub[1][:4] == 'hour' or sub[1][:2] == 'hr'):
            return sub[0] + ':00:00'
    return '0'

# Takes the summary line given and tries to parse as much html as makes sense.
# French, German, and Spanish accents html left as is.
def parseSumm(entry, line):
    line = line[line.index("000>")+4:line.rindex("</TD>")]
    line = line.replace('<BR>', '').strip()
    if line == '':
        line = 'NULL'
    entry.append(line)
 
# Contains the non-data information for the .sql file being created
def initsql():
    return ("/* Rachel Kinney and Meghan Judge\n" +
        "   rkinney4@jhu.edu\n   mjudge2@jhu.edu\n" +
        "   Databases Final Project - Parsed UFO data\n" +
        "   Data information:\n" +
        "      Source: The National UFO Reporting Center Online Database\n" +
        "      URL: http://www.nuforc.org/webreports.html\n" +
        "      Date collected: 12/06/2015 3:00 PM */\n\n" +
        "DROP TABLE IF EXISTS Sighting;\n" +
        "CREATE TABLE Sighting(\n" +
        "    SightingID    INTEGER not null,\n" +
        "    Date          DATE,\n" +
        "    Time          TIME,\n" +
        "    Shape         VARCHAR(12),\n" +
        "    Duration      TIME,\n" +
        "    Summary       TINYTEXT,\n" +
        "    primary key (SightingID));\n\n" +
        "CREATE TABLE IF NOT EXISTS Location(\n" +
        "    City          VARCHAR(25),\n" +
        "    State         CHAR(2),\n" +
        "    primary key (City, State));\n\n" +
        "DROP TABLE IF EXISTS Sighting_in;\n" +
        "CREATE TABLE Sighting_in(\n" +
        "    SightingID    INTEGER not null,\n" +
        "    City          VARCHAR(25),\n" +
        "    State         CHAR(2),\n" +
        "    primary key (SightingID),\n" +
        "    foreign key (SightingID) references Sighting(SightingID),\n" +
        "    foreign key (City, State) references Location(City, State));\n\n")

if __name__ == "__main__":
    main()
