/* 1. What is the most common shape of reported UFOs? */

SELECT Shape, Count(SightingID) AS 'Number of Sightings'
FROM Sighting
GROUP BY Shape
ORDER BY Count(SightingID) DESC;

/* 2. Compute the years in which there were the most UFO sightings, sorted by
       the number of sightings */

SELECT YEAR(Date) as Year, Count(SightingID) as NumSightings
FROM Sighting
GROUP BY YEAR(Date)
ORDER BY Count(SightingID) DESC;

/* 3. Compute the top 10 states that report the most UFO sightings, sorted by
      the number of sightings */
      
delimiter //

DROP PROCEDURE IF EXISTS TopSightingStates//

CREATE PROCEDURE TopSightingStates()
BEGIN

SELECT State, COUNT(SightingID) as 'Number of Sightings'
FROM Sighting_in
GROUP BY State
ORDER BY COUNT(SightingID) DESC LIMIT 10;
END//

/* 4. Display all UFO sightings that occurred on a specific date
      Note: Not included in report findings */

DROP PROCEDURE IF EXISTS SightingOnDate//
CREATE PROCEDURE SightingOnDate(IN mydate DATE)
BEGIN
IF EXISTS ( SELECT * from Sighting WHERE Date = mydate) THEN
	SELECT * FROM Sighting WHERE Date = mydate;
ELSE
	SELECT 'There were no reported sightings that day.' AS 'ERROR';
END IF;
END//

/* 5. Compute the month in which UFO sightings occurred most frequently between
      two years */

DROP PROCEDURE IF EXISTS MostCommonSightingMonthBetween//
CREATE PROCEDURE MostCommonSightingMonthBetween(IN year1 VARCHAR(4) , IN year2 VARCHAR(4))
BEGIN

DECLARE monthInt INT;

IF (year1 > year2) THEN
	SELECT 'INVALID YEAR ORDER' AS 'ERROR';
END IF;

SELECT MONTH(Date) INTO monthInt
FROM Sighting
WHERE YEAR(Date) <= year2 and YEAR(Date) >= year1
GROUP BY MONTH(Date)
ORDER BY COUNT(SightingID) DESC LIMIT 1;

SELECT
	CASE monthInt
		WHEN 1 then 'January'
		WHEN 2 then 'February'
		WHEN 3 then 'March'
		WHEN 4 then 'April'
		WHEN 5 then 'May'
		WHEN 6 then 'June'
		WHEN 7 then 'July'
		WHEN 8 then 'August'
		WHEN 9 then 'September'
		WHEN 10 then 'October'
		WHEN 11 then 'November'
		WHEN 12 then 'December'
	END as Month;
END//

/* 6. What is the most popular month to release a science fiction movie? */

DROP PROCEDURE IF EXISTS MostPopularSciFiMonth//
CREATE PROCEDURE MostPopularSciFiMonth()
BEGIN

DECLARE monthInt INT;

SELECT MONTH(ReleaseDate) INTO monthInt
FROM ScienceFictionMovie
GROUP BY MONTH(ReleaseDate)
ORDER BY COUNT(MovieID) DESC LIMIT 1;

SELECT
	CASE monthInt
		WHEN 1 then 'January'
		WHEN 2 then 'February'
		WHEN 3 then 'March'
		WHEN 4 then 'April'
		WHEN 5 then 'May'
		WHEN 6 then 'June'
		WHEN 7 then 'July'
		WHEN 8 then 'August'
		WHEN 9 then 'September'
		WHEN 10 then 'October'
		WHEN 11 then 'November'
		WHEN 12 then 'December'
	END as Month;
END//

/* 7. What are the peak months for changing-shape UFO sightings in California? */

DROP PROCEDURE IF EXISTS PeakMonthChangingShapeInCA//
CREATE PROCEDURE PeakMonthChangingShapeInCA()
BEGIN

SELECT CASE MONTH(Sighting.Date)
	WHEN 1 then 'January'
	WHEN 2 then 'February'
	WHEN 3 then 'March'
	WHEN 4 then 'April'
	WHEN 5 then 'May'
	WHEN 6 then 'June'
	WHEN 7 then 'July'
	WHEN 8 then 'August'
	WHEN 9 then 'September'
	WHEN 10 then 'October'
	WHEN 11 then 'November'
	WHEN 12 then 'December'
	END AS month,
COUNT(Sighting.SightingID) AS frequency
FROM Sighting, Sighting_in
WHERE Sighting.SightingID = Sighting_in.SightingID
AND Sighting.Shape = 'Changing'
AND Sighting_in.State = 'CA'
GROUP BY MONTH(Sighting.Date)
ORDER BY frequency desc;

END//
delimiter ;