/* Rachel Kinney and Meghan Judge
   rkinney4@jhu.edu / mjudge2@jhu.edu
   Databases Final Project - Full Relational Table Specifications
   
   Note: This .sql file is meant only for viewing the Rational Table Specs. For
   actually creating a sql database, use the other .sql files submited with 
   this Project. They contain the actual data and other protections. */

CREATE TABLE Sighting(
    SightingID    INTEGER not null,
    Date          DATE,
    Time          TIME,
    Shape         VARCHAR(12),
    Duration      TIME,
    Summary       TINYTEXT,
    primary key (SightingID));

CREATE TABLE IF NOT EXISTS Location(
    City          VARCHAR(25),
    State         CHAR(2),
    primary key (City, State));

CREATE TABLE Sighting_in(
    SightingID    INTEGER not null,
    City          VARCHAR(25),
    State         CHAR(2),
    primary key (SightingID),
    foreign key (SightingID) references Sighting(SightingID),
    foreign key (City, State) references Location(City, State));
    
CREATE TABLE ScienceFictionMovie(
    MovieID       INTEGER not null,
    ReleaseDate          DATE,
    MovieTitle         TINYTEXT,
    Rating          FLOAT,
    NumberVotes     INTEGER,
    primary key (MovieID));
    
/* Both military table specifications are exactly as they were found.
   See "Limitations and Improvements" in main report. */
CREATE TABLE MILITARY_INSTALLATIONS_RANGES_TRAINING_AREAS_BND (
   COMPONENT CHAR(20),
   SITE_NAME CHAR(100),
   JOINT_BASE CHAR(100),
   STATE_TERR CHAR(35),
   BRAC_SITE CHAR(16));

CREATE TABLE MILITARY_INSTALLATIONS_RANGES_TRAINING_AREAS_PT (
   COMPONENT CHAR(20),
   SITE_NAME CHAR(100),
   JOINT_BASE CHAR(100),
   STATE_TERR CHAR(35),
   BRAC_SITE CHAR(16));
   
DROP TABLE IF EXISTS FullMoon;
CREATE TABLE FullMoon(
    MoonID       INTEGER not null,
    MoonDate     DATE,
    MoonTime     TIME,
    primary key (MoonID));
